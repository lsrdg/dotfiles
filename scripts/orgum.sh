#!/bin/bash

tmux new-session -s 'orgum' -d
tmux send-keys -t 0 'nvim' C-m
tmux split-window -v 
tmux send-keys -t 1 'task' C-m
tmux split-window -h -p 50
tmux send-keys -t 2 'khal calendar today 40d' C-m
tmux attach -t 'orgum'
