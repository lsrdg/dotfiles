export EDITOR=$(which nvim)

export CDPATH=".:~/git:../"

PATH="$HOME/.node_modules/bin:$PATH"
export npm_config_prefix=~/.local
