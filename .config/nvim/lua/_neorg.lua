os_value = vim.fn.has('android')
if os_value == 1 then
	workspace_path = '/data/data/com.termux/file/home/orgumdb/neorg'
else workspace_path = '~/orgumdb/neorg'
end

print(os_value)
require('neorg').setup {
    -- Tell Neorg what modules to load
    load = {
        ["core.defaults"] = {}, -- Load all the default modules
        ["core.norg.concealer"] = {}, -- Allows for use of icons
        ["core.norg.dirman"] = { -- Manage your directories with Neorg

            config = {
                workspaces = {main = "~/orgumdb/neorg"},
                autodetect = true,
                autochdir = true
            }
        },
        ["core.norg.journal"] = {config = {journal_folder = "/journal"}},
        ["core.gtd.base"] = {
            config = {
                workspace = "main",
                context = "#contexts",
                start = "#time.start",
                due = "#time.due",
                waiting = "#waiting.for",

                projects = {
                    show_completed_projects = true,
                    show_projects_without_tasks = true
                }
            }
        },

        ["core.integrations.telescope"] = {} -- Enable the telescope module
    },

    hook = function()
        -- This sets the leader for all Neorg keybinds.

        -- Require the user callbacks module, which allows us to tap into the core of Neorg
        local neorg_callbacks = require('neorg.callbacks')

        -- Listen for the enable_keybinds event, which signals a "ready" state meaning we can bind keys.
        -- This hook will be called several times, e.g. whenever the Neorg Mode changes or an event that
        -- needs to reevaluate all the bound keys is invoked
        neorg_callbacks.on_event("core.keybinds.events.enable_keybinds",
                                 function(_, keybinds)

            -- Map all the below keybinds only when the "norg" mode is active
            keybinds.map_event_to_mode("norg", {
                n = { -- Bind keys in normal mode
                    -- Keys for managing TODO items and setting their states
                    {"gtd", "core.norg.qol.todo_items.todo.task_done"},
                    {"gtu", "core.norg.qol.todo_items.todo.task_undone"},
                    {"gtp", "core.norg.qol.todo_items.todo.task_pending"},
                    {"gtc", "core.norg.qol.todo_items.todo.task_cycle"},
                    {
                        "<LocalLeader>f",
                        "core.integrations.telescope.find_linkable"
                    }
                },
                i = {{"<C-l>", "core.integrations.telescope.insert_link"}}
            }, {silent = true, noremap = true})

        end)
    end,

    modes = {
        {name = "trace", hl = "Comment", level = vim.log.levels.TRACE},
        {name = "debug", hl = "Comment", level = vim.log.levels.DEBUG},
        {name = "info", hl = "None", level = vim.log.levels.INFO},
        {name = "warn", hl = "WarningMsg", level = vim.log.levels.WARN},
        {name = "error", hl = "ErrorMsg", level = vim.log.levels.ERROR},
        {name = "fatal", hl = "ErrorMsg", level = 5}
    }
}
