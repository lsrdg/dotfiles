local cmd = vim.cmd -- to execute Vim commands e.g. cmd('pwd')
local fn = vim.fn -- to call Vim functions e.g. fn.bufnr()
local g = vim.g -- a table to access global variables
local api = vim.api -- access api

vim.o.termguicolors = true

require('plugins')
-- Highlight on yank
vim.cmd [[
  augroup YankHighlight
    autocmd!
    autocmd TextYankPost * silent! lua vim.highlight.on_yank()
  augroup end
]]

if vim.fn.has('android') == 1 
	then 
		cmd [[colorscheme murphy]]
	else 
		cmd [[colorscheme soulf]]
end

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

vim.o.sessionoptions =
    "blank,buffers,curdir,folds,help,tabpages,winsize,winpos,terminal"

vim.api.nvim_exec([[
augroup FormatAutogroup
  autocmd!
  autocmd BufWritePost *scss,*.md,*.js,*.html,*.lua FormatWrite
augroup END
]], true)
