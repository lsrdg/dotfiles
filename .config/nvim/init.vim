filetype plugin on
syntax on

                 
" make filename-completion skip these files and directories
set wildignore+=*.swp,*.bak
set wildignore+=*.pyc,*.class,*.sln,*.cache,*.pdb,*.min.*
set wildignore+=*/.git/**/*
set wildignore+=*/min/*
set wildignore+=tags,cscope.*
set wildignore+=*.tar.*
" case-insensitive
set wildignorecase
" list files and let the user choose with the wildmenu
set wildmode=list:full
set wildcharm=<C-z>


set undofile
set undolevels=500


" Quickfix
set errorformat+=%f:%l:%c:%t:%n:%m


" statusline
set statusline=%f
set statusline+=%=  
set statusline+=%m  
"set statusline+=%{FugitiveStatusline()}
set statusline+=%-l/%L\ 
set statusline+=(%c\ %03p%%)
"%8*\ %=\ 


" CHECK
" translate shell
set keywordprg=trans\ :ja

" Splits minimals
set winheight=15
"set winminheight=5
set winwidth=30
set winminwidth=5

" Treesitter based folding
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()

if has('nvim')
  lua require('init')
endif

"------------------
" Leader
let mapleader = ","
nnoremap \\ ,
let maplocalleader = "  "

" }}}

" Colors ---------------------{{{

" set rtp+=~/git/steppenwolf.vim/
" colorscheme steppenwolf

" }}}


" Augroups ---------------------{{{

" Folding vimscript files 
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

" HTML Identing
"augroup filetype_html
"    autocmd!
"    autocmd FileType html setlocal shiftwidth=2 tabstop=2
"    autocmd BufNewFile,BufRead *.html :setlocal wrap
"    autocmd BufWritePre,BufRead *html ":execute normal! gg=G ''"
"augroup END

augroup emmet_types
    autocmd!
    autocmd FileType html,css,yml,php,markdown,htmljinja,lr,snippets,vue EmmetInstall
augroup END

augroup Linting
	autocmd!
	autocmd FileType html setlocal makeprg=prettier
	autocmd BufWritePost *.html silent make! <afile> | silent redraw!
	autocmd QuickFixCmdPost [^l]* cwindow
augroup END

" }}}

" Mappings -------------------------"{{{
" Plugins
nnoremap <space>n :Neorg 
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)


nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

"nnoremap <leader>b :buffer <C-z><S-Tab>
"nnoremap <leader>B :sbuffer <C-z><S-Tab>

" Lua print
nnoremap <leader>ll :lua print()<left>

" Last command
nnoremap <left> :<up>
nnoremap <up> :<up><cr>

" Next error in Quickfix
nnoremap <space>c :cnext<cr>

" Copy/paste from/to systems register
xnoremap <leader>y "*y
nnoremap <leader>p "*p

"in line" (entire line sans white-space; cursor at beginning--ie, ^)
xnoremap <silent> il :<c-u>normal! g_v^<cr>
onoremap <silent> il :<c-u>normal! g_v^<cr>

"around line" (entire line sans trailing newline; cursor at beginning--ie, 0)
xnoremap <silent> al :<c-u>normal! $v0<cr>
onoremap <silent> al :<c-u>normal! $v0<cr>

nnoremap <silent> <leader><leader> :FloatermToggle<CR>
tnoremap <silent> <leader><leader> <C-\><C-n>:FloatermToggle<CR>
tnoremap <silent> <leader>nf <C-\><C-n>:FloatermNew<CR>
nnoremap <silent> <space><leader> :FloatermNext<CR>
tnoremap <silent> <space><leader> <C-\><C-n>:FloatermNext<CR>

nnoremap <bs> <c-^>
" Translate shell
nnoremap <leader>ss :05split <BAR> :term <CR> trans -b :pt+da 
nnoremap <leader>sd :08split <BAR> :term <CR> trans :pt+da 

nnoremap <space><cr> i<cr><esc>
inoremap {<cr> {<cr>}<esc>O

" Buffers/Splits/Tabs/Windows ---------------------{{{
nnoremap <leader>f :find *
nnoremap <leader>F :find <C-R>=expand('%:h').'/*'<CR>

"Netrw in a vertical split
nnoremap <leader>o :20Lexplore<CR>

"Dumbnote Collection
nnoremap <leader>d :20Lexplore ~/orgumdb/dumbnote-collection<CR>

" Buffers - flow 
nnoremap <space>w :bprevious<cr>:confirm bd#<cr>
nnoremap <space><tab> :buffer *
nnoremap <space>e :Buffers<CR>
noremap <space>f :buffers<CR>:buffer<space>
nnoremap [b :bprevious<CR>
nnoremap ]b :bnext<CR>

" Splits - flow
nnoremap <space>h <C-w>h
nnoremap <space>j <C-w>j
nnoremap <space>k <C-w>k
nnoremap <space>l <C-w>l

nnoremap <C-w>v <C-w>v<C-w>w
nnoremap <C-w>s <C-w>s<C-w>w

" init.vim in a vertical split
nnoremap <F4> :vsplit $MYVIMRC<CR>
" source init.vim
nnoremap <F9> :source $MYVIMRC<CR>
" write
nnoremap <space>s :update<CR>

" Spellcheck MAPPING"{{{
nnoremap <leader>lp :set spell! spelllang=pt<CR>
nnoremap <leader>li :set spell! spelllang=en<CR>
nnoremap <leader>ld :set spell! spelllang=da<CR>
nnoremap <leader>le :set spell! spelllang=eo<CR>
nnoremap <leader>lsp :set spell! spelllang=es<CR>
nnoremap <leader>lsv :set spell! spelllang=sv<CR>
nnoremap <leader>lf :set spell! spelllang=fr<CR>
"}}}
" :terminal ------------------{{{
if has('nvim')
"tnoremap <leader>h <C-\><C-n><C-w>h
"tnoremap <leader>j <C-\><C-n><C-w>j
"tnoremap <leader>k <C-\><C-n><C-w>k
"tnoremap <leader>l <C-\><C-n><C-w>l
"tnoremap <space>w <C-\><C-n>:bprevious<cr>:bdelete#<CR>
tnoremap <C-S>a <C-\><C-n>:q<CR>
tnoremap <C-Q> <C-\><C-n>:bd!<CR>
tnoremap <ESC> <C-\><C-n>
endif
" }}}
"}}}

" Don't lose selection when shifting sidewards
xnoremap < <gv
xnoremap > >gv

" Search for the next identifier in Vim help files
nnoremap ]i :call search('\v\\|.{-}\\|')<CR>


" Enter line
nnoremap \j i<CR><ESC>
" }}}

" Functions ..................{{{ 
function! Ccursing()
  term calcurse
endfunction

function! Redir(cmd)
	for win in range(1, winnr('$'))
		if getwinvar(win, 'scratch')
			execute win . 'windo close'
		endif
	endfor
	if a:cmd =~ '^!'
		let output = system(matchstr(a:cmd, '^!\zs.*'))
	else
		redir => output
		execute a:cmd
		redir END
	endif
	vnew
	let w:scratch = 1
	setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile
	call setline(1, split(output, "\n"))
endfunction

command! -nargs=1 -complete=command Redir silent call Redir(<q-args>)


" Usage:
" 	:Redir hi ............. show the full output of command ':hi' in a scratch window
" 	:Redir !ls -al ........ show the full output of command ':!ls -al' in a scratch window

" }}}

" Commands ...................{{{
"  }}}
 
" Macros ---------------------{{{
"    -- Comment div closing tags with class' names
function! g:CloseTagComment()
  normal %
  execute "normal! yi<"
  normal %
  execute "normal! A<!-- "
  normal p
  execute "normal! A--> "
endfunction

nnoremap <leader>t :call CloseTagComment()<CR>
" }}}

