#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias nv='nvim'

alias ls='ls --color=auto'
alias ll="ls --color -lh"
alias la="ls --color -a"

PS1='[\u@\h \W$(__git_ps1 " (%s)")]\$ '

source /usr/share/git/completion/git-prompt.sh
source ~/.profile

export EDITOR=nvim

# scrap archlinux.org before full upgrade
alias iscrap='~/git/iscrap/iscrap.sh'

# git
alias gis='git status'
alias gia='git add'
alias gic='git commit -m'
alias gip='git push'
alias gid='git diff'

# yadm
alias yis='yadm status'
alias yia='yadm add'
alias yic='yadm commit -m'
alias yip='yadm push'
alias yid='yadm diff'

source /usr/share/fzf/key-bindings.bash
source /usr/share/fzf/completion.bash


# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
